import { ACTION_SESSION_SET, ACTION_SESSION_DONE } from "../actions/sessionActions"

const initialState = {
    createdAt: "",
    id: "",
    lastLogin: "",
    token: "", 
    name: "", 
    translations: "", 
    loggedIn: false 
}

export const sessionReducer = (state = initialState, action) => {
    switch (action.type){
        case ACTION_SESSION_SET: 
            return {
                ...action.payload, 
                loggedIn: true
            }
        case ACTION_SESSION_DONE: 
            return {
                initialState 
            }
        default: 
            return state
    }
}