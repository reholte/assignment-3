import { ACTION_TRANSLATIONS_SET } from "../actions/translationsActions"

const initialState = {
    translations: []
}

export const translationsReducer = (state = initialState, action) => {
    switch (action.type){
        case ACTION_TRANSLATIONS_SET: 
            return {
                ...action.payload
            }
        default: 
            return state
    }
}