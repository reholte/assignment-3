import { combineReducers } from "redux";
import { sessionReducer } from "./sessionReducer"; 
import { translationsReducer } from "./translationsReducer";

const appReducer = combineReducers({
    translationsReducer,
    sessionReducer

})

export default appReducer