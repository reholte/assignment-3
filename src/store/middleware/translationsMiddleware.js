import { updateTranslations } from "../../components/Home/TranslationsAPI";
import {
	
	ACTION_TRANSLATIONS_ATTEMPT,
	ACTION_TRANSLATIONS_SET,
	translationsSetAction,
} from "../actions/translationsActions";
import { sessionSetAction } from "../actions/sessionActions";

// Sends to updateTranslations in TranslationsAPI if user trying to translate, sends to sessionSetAction when translation is set 
export const translationsMiddleware = ({ dispatch }) => (next) => (action) => { 
		next(action);
		
		if (action.type === ACTION_TRANSLATIONS_ATTEMPT) {
			updateTranslations(action.payload)
			 .then(response => {
				 dispatch(translationsSetAction(response)); // response will be the user object with updated translations
			 })
		 }
		 if (action.type === ACTION_TRANSLATIONS_SET) {
			dispatch(sessionSetAction(action.payload));
		}
	};

