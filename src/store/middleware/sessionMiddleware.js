import { ACTION_SESSION_INIT, ACTION_SESSION_SET, ACTION_SESSION_DONE,  sessionSetAction, sessionDoneAction } from "../actions/sessionActions";

// Different sessions for storing and getting to/from local storage 
export const sessionMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_SESSION_INIT){
        const storedSession = localStorage.getItem('lt-ss'); 
        if (!storedSession){
            return 
        }
        const session = JSON.parse(storedSession);
        dispatch(sessionSetAction(session));
    }

    if (action.type === ACTION_SESSION_SET){
        localStorage.setItem('lt-ss', JSON.stringify(action.payload));
    }

    if (action.type === ACTION_SESSION_DONE){
        const storedSession = localStorage.getItem('lt-ss');
        if (!storedSession){
            return 
        }
        const session = JSON.parse(storedSession);
        dispatch(sessionDoneAction(session));
    }
}