import { applyMiddleware } from "redux";
import { loginMiddleware } from "./loginMiddleware";
import { sessionMiddleware } from "./sessionMiddleware";
import { translationsMiddleware } from "./translationsMiddleware";

export default applyMiddleware(
    loginMiddleware, 
    translationsMiddleware,
    sessionMiddleware
)