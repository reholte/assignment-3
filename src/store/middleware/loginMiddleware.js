import { LoginAPI } from "../../components/Login/LoginAPI";
import {
	ACTION_LOGIN_SUCCESS,
	ACTION_LOGIN_ATTEMPT,
    loginSuccessAction
} from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionActions";

// Sends to login in LoginAPI if user trying to login, sends to sessionSetAction if login success
export const loginMiddleware =
	({ dispatch }) =>
	(next) =>
	(action) => {
		next(action);

		if (action.type === ACTION_LOGIN_ATTEMPT) {
			LoginAPI.login(action.payload)
				.then((profile) => {
					dispatch(loginSuccessAction(profile));
				})
				.catch((error) => {
					console.log(error.message);
				});
		}

		if (action.type === ACTION_LOGIN_SUCCESS) {
			dispatch(sessionSetAction(action.payload));
		}
	};
