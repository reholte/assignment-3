export const ACTION_TRANSLATIONS_ATTEMPT = '[translations] ATTEMPT';
export const ACTION_TRANSLATIONS_SET = '[translations] SET';
export const ACTION_TRANSLATIONS_UPDATE = '[translations] UPDATE';

export const translationsAttemptAction = credentials => ({
    type: ACTION_TRANSLATIONS_ATTEMPT,
    payload: credentials
})


export const translationsSetAction = profile => ({
    type: ACTION_TRANSLATIONS_SET, 
    payload: profile
})

export const translationsUpdateAction = profile => ({
    type: ACTION_TRANSLATIONS_UPDATE, 
    payload: profile 
})