export const handleFirstResponse = async response => {
    if (!response.ok){
        const { error = 'Oops, something went wrong' } = await response.json()
        throw new Error(error)
    }
    return response.json()
}   