import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AppContainer from "./hoc/AppContainer";
import Login from "./components/Login/Login";
import Home from "./components/Home/Home";
import NotFound from "./components/NotFound/NotFound";
import Navbar from "./components/Navbar";
import Profile from "./components/Profile/Profile";

function App() {
	return (
		<BrowserRouter>
			<Navbar />
			<div className="App">
				<AppContainer>
					<h1>
						<img 
							src={"assets/Logo-Hello.png"}
							alt="Logo hello"
							width="100"
						></img>
						<span>Language Translator</span>
						
						</h1>
				</AppContainer>
				<Switch>
					<Route path="/" exact component={Login}></Route>
					<Route path="/home" component={Home}></Route>
					<Route path="/profile" component={Profile}></Route>
					<Route path="*" component={NotFound}></Route>
				</Switch>
			</div>
		</BrowserRouter>
	);
}

export default App;
