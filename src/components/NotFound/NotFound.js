import { Link } from 'react-router-dom'; 
//Page when user tries to access a page that doesn't exist 
function NotFound(){
    return (
        <main>
            <h4>Wait a minute!</h4>
            <p>You're trying to access a page that doesn't exist. Please go home</p>
            <Link to="/">CLICK HERE TO GO HOME</Link>
        </main>
    )
}
export default NotFound;