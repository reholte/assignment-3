import { roundToNearestMinutes } from "date-fns/esm";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import { loginAttemptAction } from "../../store/actions/loginActions";

function Login() {
	const dispatch = useDispatch();
	const { loggedIn } = useSelector((state) => state.sessionReducer);

	const [name, setName] = useState([]);

	// Function that excecutes when user types in input-section 
	const onInputChange = (event) => {
		setName({
			...roundToNearestMinutes,
			[event.target.id]: event.target.value,
		});
	};

	// Function that excecutes when form is submittet, aka when user press button 
	const onFormSubmit = (event) => {
		event.preventDefault();
		dispatch(loginAttemptAction(name));
	};

	return (
		<>
			{loggedIn && <Redirect to="/Home" />}
			{!loggedIn && (
				<AppContainer>
					<form className="mt-3 mb-3" onSubmit={onFormSubmit}>
						<h1>Get started</h1>
						<p>Write your name to login 🐧</p>
						<div className="mb-3">
							<label htmlFor="name" className="form-label">
								Name:{" "}
							</label>
							<input
								id="name"
								type="text"
								placeholder="What's your name?"
								className="form-control"
								onChange={onInputChange}
							/>
						</div>
						<button type="submit" className="btn btn-primary btn-lg">
							GO!
						</button>
					</form>
				</AppContainer>
			)}
		</>
	);
}

export default Login;
