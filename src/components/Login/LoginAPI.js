// Login API edits 
export const LoginAPI = {
    
    async login(object){
        const name = object.name
        const existingUser = await checkUser(name)
        if (existingUser.length) {
            return new Promise(resolve => resolve(existingUser.pop()))
        }
        return fetch('http://localhost:8999/users', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
                name,
                translations: []
            })
        })
        .then(handleFirstResponse)
    }   
}

// Checks if user already exists 
async function checkUser(name){
    return fetch('http://localhost:8999/users?name='+name)
        .then(handleFirstResponse)
}

function handleFirstResponse(response) {
    return response.json()
}