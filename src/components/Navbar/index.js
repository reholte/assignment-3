import NavDropdown from "react-bootstrap/NavDropdown";
import { useSelector } from "react-redux";

// Clear local storage to log out  
function logOut() {
	localStorage.clear();
}

// Navbar that exists on all pages, has two versions, one when user is logged in, one when user is not logged in 
function Navbar() {
	const { loggedIn } = useSelector((state) => state.sessionReducer);
	const { name } = useSelector((state) => state.sessionReducer); 
	return (
		<>
			{loggedIn && (
				<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
					<div className="container-fluid">
						<img src={ `assets/Logo.png` } alt="Logo" width="50"></img>
						<a className="navbar-brand" href="/">
							Language Translator
						</a>
						<NavDropdown title={name} id="basic-nav-dropdown">
							<NavDropdown.Item href="profile">Profile</NavDropdown.Item>
							<NavDropdown.Item href="home">Home</NavDropdown.Item>
							<NavDropdown.Divider />
							<NavDropdown.Item href="/" onClick={() => logOut()}>Log out</NavDropdown.Item>
						</NavDropdown>
					</div>
				</nav>
			)}
			{!loggedIn && (
				<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
					<div className="container-fluid">
						<img src={ `assets/Logo.png` } alt="Logo" width="50"></img>
						<a className="navbar-brand" href="/">
							Language Translator
						</a>
						<p>   </p>
					</div>
				</nav>
			)}
		</>
	);
}

export default Navbar;
