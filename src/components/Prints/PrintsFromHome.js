// Function that show signs on translation page 
function PrintsFromHome(translations) {
	let images = findSigns(translations);

	return (
		<>
		{images}
		</>
	)
}
// Function that find signs 
function findSigns(translations) {
	let word = "";
	// If no word, Holiday is standard 
	if (typeof(translations.translations) !== 'string') {
		word = "Holiday";
	} else {
		word = translations.translations;
	}
	word = word.toLowerCase();
	let chars = word.split("");
	let images = [];
	for (let char of chars) {
		let id = "id" + Math.random(); 
		images.push(<img key={id} src={"assets/individial_signs/"+char + ".png"} alt="Sign" width="100"></img>);
	}
	return images;
}

export default PrintsFromHome;
