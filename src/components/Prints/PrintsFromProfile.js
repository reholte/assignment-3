// Function that returns list to show (10) last translations 
function PrintsFromProfile(translations) {
    let elements = []; 
    if (translations.length >= 10){
        for (let i = 1; i < 11; i++){
            let id = "id" + i; 
            elements.push(<li key={id} >{translations[translations.length-i]}</li>)
        }
    }
    else {
        let length = translations.length; 
        for (let i = 1; i < length +1; i++){
            elements.push(<li>{translations[length-i]}</li>)
        }

    }
    return (
    <>
    {elements}
    </>
    )
}

export default PrintsFromProfile;