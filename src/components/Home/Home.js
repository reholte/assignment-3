import AppContainer from "../../hoc/AppContainer";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { translationsAttemptAction } from "../../store/actions/translationsActions";
import Prints from "../Prints/PrintsFromHome"

// Component for the Home-page aka the translation-page 
const Home = () => {
	const dispatch = useDispatch();
	const [translations, setTranslation] = useState([]);

	const user = useSelector((state) => state.sessionReducer);

	// Function that excecutes when user types in input-section 
	const onInputChange = (event) => {
		setTranslation(event.target.value);
	};

	// Function that excecutes when form is submittet, aka when user press enter 
	const onFormSubmit = (event) => {
		event.preventDefault();
		dispatch(
			translationsAttemptAction({
				...user,
				translations: [...user.translations, translations],
			})
		);
	};

	return (
		<main>
			<AppContainer>
				<form className="mt-3 mb-3" onSubmit={onFormSubmit}>
					<h1>Welcome to the sign-language translator</h1>
					<p>Type in a word here to see that word in sign-language</p>
					<p>To store the word in your database you need to PRESS ENTER</p>
					<div className="mb-3">
						<input
							id="translations"
							type="text"
							className="form-control"
							placeholder="Example: Holiday"
							onChange={onInputChange}
						></input>
					</div>
				</form>
					<div id="signs">{Prints({ translations })}</div>
			</AppContainer>
		</main>
	);
};

export default Home;
