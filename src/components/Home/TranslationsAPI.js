// Translations API. Updates translations in the API 
export function updateTranslations(user) {
    return fetch("http://localhost:8999/users/" + user.id, {
        method: "PATCH",
        headers: {
            "content-type": "application/json",
        },
        body: JSON.stringify(user), 
    }).then(handleFirstResponse);
}

function handleFirstResponse(response) {
	return response.json();
}

