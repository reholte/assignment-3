import AppContainer from "../../hoc/AppContainer";
import { useDispatch, useSelector } from "react-redux";
import PrintsFromProfile from "../Prints/PrintsFromProfile";
import { translationsAttemptAction } from "../../store/actions/translationsActions";

// Profile page
const Profile = () => {
	const { name } = useSelector((state) => state.sessionReducer);

	const user = useSelector((state) => state.sessionReducer);
	const translations = user.translations;

	const dispatch = useDispatch();

	// Function that excecutes when form is submittet, aka when user press button 
	const onFormSubmit = (event) => {
		event.preventDefault();
		dispatch(
			translationsAttemptAction({
				...user,
				translations: [],
			})
		);
	};

	return (
		<main>
			<AppContainer>
			<form className="mt-3 mb-3" onSubmit={onFormSubmit}>
				<h1>Hello, {name}</h1>
				<p>Your 10 last translations</p>
				<div className="mb-3">
					{PrintsFromProfile(translations)}
				</div>
				<button type="submit" className="btn btn-primary btn-lg">
					Delete!
				</button>
			</form>
			</AppContainer>
		</main>
	);
};

export default Profile;
