# Assignment 3
## Ragnhild Emblem Holte 

## To start the project: 
Terminal 1: `json-server --watch users.json --port 8999`
Terminal 2: `npm start`

### Special thanks to: 
Dewald Els for helping me with understanding react and giving me tips to solve problems that I encountered


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `json-server --watch users.json --port 8999`

Opens up json-server db in the right port  

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

